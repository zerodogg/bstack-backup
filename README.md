# bstack-backup

A small program that can generate backups of an instance of the [BookStack
wiki](https://www.bookstackapp.com/) software.  It uses the API provided by
BookStack to perform exports of your BookStack instance.

## Usage
```
  bstack-backup [--format=<SupportedFormats>] [--overwrite] <URL> <api-token-id> <api-token-secret>
  bstack-backup [--version]
  
    <URL>                          The base URL of your BookStack instance.
    <api-token-id>                 The "ID" part of your API token.
    <api-token-secret>             The secret part of your API token.
    --format=<SupportedFormats>    pdf, txt or html. Defaults to pdf.
    --overwrite                    Rename existing files to file~ if they already exist.
    --version                      Output version information and exit.

```

### API token

To get your API token select "edit profile" within BookStack. At the bottom of
that page you can generate API tokens.

## Installation

`bstack-backup` is written in [Raku](https://raku.org). If your distribution
does not have up-to-date raku packages you can find repositories for most
distributions (Fedora, Debian, Ubuntu, ..) at
[rakudo-pkg](https://github.com/nxadm/rakudo-pkg#os-repositories).  Once you
have raku installed, you can install the modules that bstack-backup uses by
running `zef install --deps-only .`. Once you have done that, you can run
`./bstack-backup`. If you want to install it into your PATH you can do `zef
install .`.

### Installation troubleshooting

**IO::Socket::SSL won't install (Fedora)**  
You're missing the package `compat-openssl10-devel`.

**zef is missing (using rakudo-pkg)**  
Run `/opt/rakudo-pkg/bin/install-zef`.

## Credits

Thanks to [Dan Brown](https://danb.me/) for a [PHP
script](https://gist.github.com/ssddanbrown/45acb913a7b873240b2d89781e74a7a4)
that also exports from BookStack. It was helpful as a reference implementation
for the API.

Thanks to all the [BookStack
contributors](https://github.com/BookStackApp/BookStack/graphs/contributors)
for writing an excellent wiki.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
