DISTFILES=README.md bin COPYING Makefile META6.json
VERSION:=$(shell ./bin/bstack-backup --version 2>/dev/null|perl -p -e 's/^\D+//; chomp')

help:
	@echo "Available targets:"
	@echo "install     - Install bstack-backup and dependencies with zef"
	@echo "deps        - Install dependencies only with zef"
install:
	zef install .
deps:
	zef install --deps-only .
clean:
	rm -rf bstack-backup-$(VERSION)
	rm -f bstack-backup-$(VERSION).tar.*
# Create the tarball
distrib: clean readme
	mkdir -p bstack-backup-$(VERSION)
	cp -r $(DISTFILES) ./bstack-backup-$(VERSION)
	cd bstack-backup-$(VERSION) && ln -s bin/bstack-backup .
	tar -jcvf bstack-backup-$(VERSION).tar.bz2 ./bstack-backup-$(VERSION)
	rm -rf bstack-backup-$(VERSION)
# Update the README
readme:
	raku -e 'my $$r = ".README.tpl.md".IO.slurp; my $$u = run("./bstack-backup", "--help", :out).out.slurp(:close); $$u ~~ s:g/\.\///; $$u ~~ s:m/Usage\:\n//; $$r ~~ s/USAGE_CONTENT/$$u/; print $$r;' > README.md
